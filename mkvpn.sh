set -e

vpnname="$1"
vpnnumber="$2"
vpnhost="$3"
vpnport="$4"

if test -z "$vpnname" || test -z "$vpnnumber" || test -z "$vpnhost" || test -z "$vpnport"
then
	echo "Usage: mkvpn <vpn name> <vpn number> <vpn host> <vpn port>"
	exit 1
fi

vpndir="/etc/openvpn/$vpnname"
if test -d "$vpndir"
then
	echo "VPN $vpnname already present!"
	exit 1
fi

mkdir -p "$vpndir"
cd "$vpndir"
cat >> "vars" <<-'EOF'
	set_var EASYRSA_ALGO ed
	set_var EASYRSA_CURVE ed25519
	set_var EASYRSA_CA_EXPIRE 3650
	set_var EASYRSA_CERT_EXPIRE 3650
	set_var EASYRSA_BATCH yes
EOF
easyrsa --vars=./vars init-pki
easyrsa --vars=./vars build-ca nopass
easyrsa --vars=./vars build-server-full "${vpnname}" nopass
openvpn --genkey --secret ta.key
SERVER_CONF="mode server                                                                                                                                                    
tls-server
proto udp
dev tun_${vpnname}
port ${vpnport}

ca pki/ca.crt
cert pki/issued/${vpnname}.crt
key pki/private/${vpnname}.key

server 192.168.${vpnnumber}.0 255.255.255.0
topology subnet
client-config-dir clients
ifconfig-pool-persist ip
keepalive 10 40 

ping-timer-rem

mssfix
mtu-disc yes
persist-key
persist-tun
persist-local-ip
persist-remote-ip
float

cipher AES-128-GCM
tls-ciphersuites TLS_CHACHA20_POLY1305_SHA256
tls-cipher TLS-ECDHE-ECDSA-WITH-CHACHA20-POLY1305-SHA256
dh none
tls-version-min 1.2
tls-auth ta.key 0
tls-groups secp384r1
auth SHA512

script-security 1

client-to-client
txqueuelen 1000
"

echo "$SERVER_CONF" > server.conf
mkdir -p clients
mkdir -p client-configs

CLIENTCONF="remote $vpnhost
resolv-retry infinite
nobind
port $vpnport
tls-client
proto udp
dev tun_$vpnname
pull
persist-key
persist-tun

remote-cert-tls server

cipher AES-128-GCM
tls-ciphersuites TLS_CHACHA20_POLY1305_SHA256
tls-cipher TLS-ECDHE-ECDSA-WITH-CHACHA20-POLY1305-SHA256
dh none
tls-version-min 1.2
tls-auth ta.key 0
tls-groups secp384r1
auth SHA512

key-direction 1

mssfix
keepalive 10 40
"
echo "$CLIENTCONF" > client.conf
