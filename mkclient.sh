set -e

vpnname="$1"
clientname="$2"

if test -z "$vpnname" || test -z "$clientname"
then
	echo "Usage: mkclient <vpn name>"
	exit 1
fi

vpndir="/etc/openvpn/$vpnname"
if ! test -d "$vpndir"
then
	echo "VPN $vpnname not present!"
	exit 1
fi

cd "$vpndir"
easyrsa --vars=./vars build-client-full "$clientname" nopass

inline() {
	cfg="$1"
	fname="$2"
	echo "<$1>"
	cat "$2"
	echo "</$1>"
}

clientconf="client-configs/${clientname}.ovpn"
mkdir -p client-configs
touch "$clientconf"
chmod a-rw,g-rw,u+rw "$clientconf"

(cat client.conf; inline "tls-auth" "ta.key"; inline "ca" "pki/ca.crt"; inline "cert" "pki/issued/${clientname}.crt"; inline "key" "pki/private/${clientname}.key") > "$clientconf"

echo "New client created: $(readlink -f "$clientconf")"
